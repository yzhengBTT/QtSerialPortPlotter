#include "settings.h"
#include <QSettings>
#include <QScreen>
#include "mainwindow.hpp"

Settings::Settings(QObject *parent) : QObject(parent)
{

}

QString Settings::getPortName(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("portName", "").toString();
}

int Settings::getBaudrate(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("baudrate", 9600).toInt();
}

int Settings::getDatabits(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("databits", -1).toInt();
}

int Settings::getStopbits(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("stopbits", -1).toInt();
}

int Settings::getParity(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("parity", -1).toInt();
}

int Settings::getTheme(){
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("theme", 0).toInt();
}

QStringList Settings::getLineColors()
{
    QStringList l = {"#c0504d", "#9bbb59", "#8064a2", "#4bacc6", "#f79646", "#4f81bd"};
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("lineColors", l).toStringList();
}

QStringList Settings::getLineNames()
{
    QStringList l;
    for(int i = 0; i != 10; i++)
    {
        l.append(tr("通道 %1").arg(i + 1));
    }
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("lineNames", l).toStringList();
}

bool Settings::hideRecvArea()
{
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("hideRecvArea", false).toBool();
}

bool Settings::hideRecvData()
{
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("hideRecvData", false).toBool();
}

bool Settings::displayRawData()
{
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("displayRawData", false).toBool();
}

QSize Settings::windowSize()
{
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("windowSize", QSize(WINDOW_DEFAULT_WIDTH, WINDOW_DEFAULT_HEIGHT)).toSize();
}

QPoint Settings::windowPos()
{
    QScreen *screen = qApp->primaryScreen();
    int w = screen->size().width();            // 屏幕宽
    int h = screen->size().height();           // 屏幕高
    int x = (w - WINDOW_DEFAULT_WIDTH) / 2;
    int y = (h - WINDOW_DEFAULT_HEIGHT) / 3;
    x = x < 0 ? 0 : x;
    y = y < 0 ? 0 : y;
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("windowPos", QPoint(x, y)).toPoint();
}

QList<int> Settings::plotArgs()
{
    QList<int> l = {100, 10, 0, 100};
    QStringList keys = {"points", "setup", "min", "max"};
    QSettings settings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    settings.beginReadArray("plotArgs");
    settings.setArrayIndex(0);
    for(int i = 0; i < 4; i++)
    {
        l[i] = settings.value(keys[i], l[i]).toInt();
    }
    settings.endArray();
    return l;
}

QString Settings::lastSavePath()
{
    return QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).value("lastSavePath", QApplication::applicationDirPath()).toString();
}

void Settings::setPlotArgs(QList<int> l)
{
    QStringList keys = {"points", "setup", "min", "max"};
    QSettings settings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    settings.beginWriteArray("plotArgs");
    settings.setArrayIndex(0);
    int count = l.size() > 4 ? 4 : l.size();
    for(int i = 0; i < count; i++)
    {
        settings.setValue(keys[i], l[i]);
    }
    settings.endArray();
}

void Settings::setLastSavePath(const QString &path)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("lastSavePath", path);
}

void Settings::setPortName(QString portName){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("portName", portName);
}

void Settings::setBaudrate(int baudrate){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("baudrate", baudrate);
}

void Settings::setDatabits(int index){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("databits", index);
}

void Settings::setStopbits(int index){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("stopbits", index);
}

void Settings::setParity(int index){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("parity", index);
}

void Settings::setTheme(int index){
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("theme", index);
}

void Settings::setLineColors(const QStringList &l)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("lineColors", l);
}

void Settings::setLineNames(const QStringList l)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("lineNames", l);
}

void Settings::setHideRecvArea(bool enable)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("hideRecvArea", enable);
}

void Settings::setHideRecvData(bool enable)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("hideRecvData", enable);
}

void Settings::setDisplayRawData(bool enable)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("displayRawData", enable);
}

void Settings::setWindowPos(QPoint p)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("windowPos", p);
}

void Settings::setWindowSize(QSize s)
{
    QSettings(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat).setValue("windowSize", s);
}
